# TreeMaker

## Author: Kevin Hildebrand
## Email: khildebrand@uchicago.edu

This package is for creating trees from DAODs.

First time setup. Run commands (From within TreeMaker directory):

rcSetup Base,2.4.17
rc find_packages
rc compile

Each time after this when you log in from a new terminal all that is required is the command "rcSetup". You also need to setup Rucio,your grid proxy and panada. (localSetupRucioClient,voms-proxy-init -voms atlas, localSetupPandaClient)

## QuickStart quide (lets just get this thing to run).

If you are reading this then I assume you have checked out this package succesfully and have already setup atlas (assuming you are working on lxplus this means running the command "setupATLAS").

From with the TreeMaker directory:

```
localSetupRucioClients
"voms-proxy-init -voms atlas
```
enter your password for your grid certificate.

```
localSetupPandaClient
rcSetup Base,2.4.17
rc find_packages
rc compile
```
hopefully everything compiled correctly.

Open the files steerTreeMaker.cxx and steerTreeMakerData.cxx located in the run directory and look for the two lines in each "user.khildebr.test." and change them to "user.<your cern id>.test.".
In steerTreeMaker.cxx change `runOnGrid=true` to `runOnGrid=false`.
In steerTreeMaker.cxx change line 26 (	  SH::readFileList (sh, "sample", "/home/khildebr/qualTask/filelist/filelist.txt");). Change "/home/khildebr/qualTask/filelist/filelist.txt" to the full path of filelist.txt located in the TreeMaker directory.

Make sure the permission on runsteerTreeMaker.sh is set to executable for you.
Then from within the run directory run this command:
"./runsteerTreeMaker.sh Test v1"

Once the script finishes running (assuming it runs ok) a directory Test should now exist within the run directory containing your output tree.


## What this package does:

The Random Cone package takes DAODs/xAODs and produces trees from them. They are ran using scripts located in the run folder.
The name is misleading and this package has actually nothing to do with Random Cones and is just a script that makes trees.

## How to configure this package:

Configuration of this package is set in a config file. This file allows one to set the following:
1. name of the output tree
2. what branches you want in the tree. Currently supported are:
   * up to 3 different xAOD::JetContainer with the option of including there constituents (an example would be adding this line to the config script "JetContainerName AntiKt4EMTopoJets")
   * a xAOD::CaloClusterContainer (an example would be adding this line to the config script "CaloClusterContainerName	CaloCalTopoClusters")
   * a xAOD::VertexContainer (an example would be adding this line to the config script "PrimaryVertexContainerName	PrimaryVertices")
   * a xAOD::TruthVertexContainer (an example would be adding this line to the config script "TruthPrimaryVertexContainerName	TruthVertices")
   * a xAOD::TrackParticleContainer (an example would be adding this line to the config script "TrackParticleContainerName	InDetTrackParticles")
   * a xAOD::TruthParticleContainer (an example would be adding this line to the config script "TruthParticleContainerName	TruthParticles")
3. Whether or not to do pileup reweighting (default is false,example in config script "DoPileupReweighting	True"). If this is set to true then you must provide the LumiCalcFiles and PRWFiles.
4. What LumiCalcFiles and PRWFiles to use for pileup reweighting (exampels "LumiCalcFiles	$ROOTCOREBIN/data/RandomCone/ilumicalc_histograms_HLT_noalg_zb_L1ZB_276262-284484_OflLumi-13TeV-005.root" and "PRWFiles	$ROOTCOREBIN/data/RandomCone/JZ0W_JZ1Wv30.prw.root")
5. what DataScaleFactor to use for PRW tool. You are passing it the denominator. So if you want the DataScaleFactor to be 1.0/1.16 you would set this in the config script to 1.16 (default is 1. example in config script "DataScaleFactor 1.16").
6. what DataScaleFactorUP to use for PRW tool. You are passing it the denominator. So if you want the DataScaleFactorUP to be 1.0/1.16 you would set this in the config script to 1.16. If you want it set to zero pass it any negative value, eg -1. (default is 0 example in config script "DataScaleFactorUP 1.16").
7. what DataScaleFactorDOWN to use for PRW tool. You are passing it the denominator. So if you want the DataScaleFactorDOWN to be 1.0/1.16 you would set this in the config script to 1.16. If you want it set to zero pass it any negative value, eg -1. (default is 0 example in config script "DataScaleFactorDOWN 1.16").
8. Whether or not to use GRL (example "ApplyGRLCut	True"). If this is set to true then you must provide the GRL.
9. What GRL file to use for Applying GRL cut (example "GRL	$ROOTCOREBIN/data/RandomCone/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml").

## How to run this package:

Assuming you have everything compiled and configured correctly. The package is simply run using scripts in the run directory.

The scripts steerTreeMaker.cxx and steerTreeMakerData.cxx are almost identical, there only difference is what data sets are run on.

For testing purposes this package can be run locally by changing the flag, `runOnGrid`, near the top of steerTreeMaker.cxx to false.
If runOnGrid is set to false then the input dataset ran on will be taken from file defined on line 26 

```SH::readFileList (sh, "sample", "/home/khildebr/qualTask/filelist/filelist.txt");"```

You will need to change this to your own file location. Also when running locally it will only run over the first 1000 events.
If runOnGrid is set to true then the input datasets will be taken from rucio. See lines 31 to 136 in steerTreeMakerData.cxx for examples.

The config script used to congfigure this package is set in steerTreeMaker.cxx and steerTreeMakerData.cxx. 
The default is tree.config in RandomCone/data. If you want to change this look for the line in the steering script containing "setConfig" and change it as desired.

Things you need to change in steerTreeMaker.cxx and steerTreeMakerData.cxx so you can run the package. 
The script is currently set to run on my own account (khildebr), so you will need to change it to your own account. 
At the end of steerTreeMaker.cxx and steerTreeMakerData.cxx look for the two lines containing "user.khildebr.test.". 
Change `khildebr` here to your own cern id. ie `user.<cern id>.`
Note this isn't really neccessary for running locally but is when running on the grid.

The steering scripts steerTreeMaker.cxx and steerTreeMakerData.cxx take two inputs. 
The first is where you want to save the output to `"/full/path/of/output/"` 
and the second is what version tag you want to append to the end of the output (eg "v1").
If the ouput directory already exists and you are submitting to the grid then the script will not run to avoid overwriting existing output.
The version tag needs to be different for each succesfull grid submission of the same input dataset otherwise the grid will see that this job has already been submitted and will not run it again.

To run the steering script make sure runsteerTreeMaker and runsteerTreeMakerData
have their permissions set to executable and from within the run directory just type the command:
```./runsteerTreeMaker /full/path/of/output/ v1```
or 
```./runsteerTreeMakerData /full/path/of/output/ v1```

This will run the script. If running locally the output will appear in `/full/path/of/output/` once the script has finished running. 
If running on the grid you should receive an email when the job finishes running. 
You will then need to download the output from the grid to the directory `/full/path/of/output/` yourself.
The script `retrieveGridRuciob.sh` is an example of a script that I use to download the output from the grid.

If you are doing the Random Cone noise study and using my other packages that come after this one then 
the only branches you need and that are required in the tree are `CaloClusterContainerName` and `PrimaryVertexContainerName`.