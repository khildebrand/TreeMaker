#ifndef RandomCone_treeMaker_H
#define RandomCone_treeMaker_H

#//include <EventLoop/Algorithm.h>

// algorithm wrapper
#include "RandomCone/Algorithm.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PileupReweighting/PileupReweightingTool.h"

//Infrastructure includes
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <TH1.h>
#include <TTree.h>

class treeMaker : public RC::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;
  int m_eventCounter; //!

  float eventWeight; //!



  float nEvents; //!
  std::string treeName; //!
  std::string JetContainerName; //!
  std::string JetContainerName2; //!
  std::string JetContainerName3; //!
  std::string CaloClusterContainerName; //!
  std::string PrimaryVertexContainerName; //!
  std::string TruthPrimaryVertexContainerName; //!
  std::string TrackParticleContainerName; //!
  std::string TruthParticleContainerName; //!
  double dataScaleFactor; //!
  double dataScaleFactorUP; //!
  double dataScaleFactorDOWN; //!
  bool JetContainerNameDoConstituent;
  bool JetContainerName2DoConstituent;
  bool JetContainerName3DoConstituent;


  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  std::string outputName;
  TTree *tree; //!
  TH1F *prwHistogram;//!
  TH1F *ilumiHistogram;//!
  int EventNumber; //!
  std::vector<float> Jet_width; //!
  std::vector<float> Jet_rapidity; //!
  std::vector<float> Jet_e; //!
  std::vector<float> Jet_pt; //!
  std::vector<float> Jet_eta; //!
  std::vector<float> Jet_phi; //!
  std::vector<float> Jet_m; //!
  int Jet_n; //!
  std::vector<std::vector<float>> JetConstituent_rapidity; //!
  std::vector<std::vector<float>> JetConstituent_e; //!
  std::vector<std::vector<float>> JetConstituent_pt; //!
  std::vector<std::vector<float>> JetConstituent_eta; //!
  std::vector<std::vector<float>> JetConstituent_phi; //!
  std::vector<std::vector<float>> JetConstituent_m; //!

  std::vector<float> Truth_Jet_width; //!
  std::vector<float> Truth_Jet_rapidity; //!
  std::vector<float> Truth_Jet_e; //!
  std::vector<float> Truth_Jet_pt; //!
  std::vector<float> Truth_Jet_eta; //!
  std::vector<float> Truth_Jet_phi; //!
  std::vector<float> Truth_Jet_m; //!
  int Truth_Jet_n; //!

  std::vector<std::vector<float>> Jet3Constituent_rapidity; //!
  std::vector<std::vector<float>> Jet3Constituent_e; //!
  std::vector<std::vector<float>> Jet3Constituent_pt; //!
  std::vector<std::vector<float>> Jet3Constituent_eta; //!
  std::vector<std::vector<float>> Jet3Constituent_phi; //!
  std::vector<std::vector<float>> Jet3Constituent_m; //!


  std::vector<float> JetTk_width; //!
  std::vector<float> JetTk_rapidity; //!
  std::vector<float> JetTk_e; //!
  std::vector<float> JetTk_pt; //!
  std::vector<float> JetTk_eta; //!
  std::vector<float> JetTk_phi; //!
  std::vector<float> JetTk_m; //!
  int JetTk_n; //!
  std::vector<std::vector<float>> Jet2Constituent_rapidity; //!
  std::vector<std::vector<float>> Jet2Constituent_e; //!
  std::vector<std::vector<float>> Jet2Constituent_pt; //!
  std::vector<std::vector<float>> Jet2Constituent_eta; //!
  std::vector<std::vector<float>> Jet2Constituent_phi; //!
  std::vector<std::vector<float>> Jet2Constituent_m; //!

  //caloCluster
  std::vector<float> CaloCluster_EM_rapidity; //!
  std::vector<float> CaloCluster_EM_pt; //!
  std::vector<float> CaloCluster_EM_e; //!
  std::vector<float> CaloCluster_EM_et; //!
  std::vector<float> CaloCluster_EM_phi; //!
  std::vector<float> CaloCluster_EM_eta; //!
  std::vector<float> CaloCluster_EM_m; //!

  std::vector<float> CaloCluster_LC_rapidity; //!
  std::vector<float> CaloCluster_LC_pt; //!
  std::vector<float> CaloCluster_LC_e; //!
  std::vector<float> CaloCluster_LC_et; //!
  std::vector<float> CaloCluster_LC_phi; //!
  std::vector<float> CaloCluster_LC_eta; //!
  std::vector<float> CaloCluster_LC_m; //!

  int CaloCluster_n; //!

  float EventWeight; //!
  float PileupWeight; //!
  float PileupWeight_Kevin; //!

  int Primary_Vertices_n; //!
  //std::vector<float> Primary_Vertices_x; //!
  //std::vector<float> Primary_Vertices_y; //!
  std::vector<float> Primary_Vertices_z; //!
  std::vector<int> Primary_Vertices_type; //!
  std::vector<int> Primary_Vertex_nTracks; //!

  int Truth_Primary_Vertices_n; //!
  //std::vector<float> Primary_Vertices_x; //!
  //std::vector<float> Primary_Vertices_y; //!
  std::vector<float> Truth_Primary_Vertices_z; //!
  //std::vector<int> Truth_Primary_Vertices_type; //!

  std::vector<float> TrackParticle_phi; //!
  std::vector<float> TrackParticle_theta; //!
  std::vector<float> TrackParticle_vz; //!
  std::vector<float> TrackParticle_z0; //!
  std::vector<float> TrackParticle_d0; //!
  std::vector<float> TrackParticle_charge; //!

  std::vector<float> TrackParticle_pt; //!
  std::vector<float> TrackParticle_eta; //!
  //std::vector<float> TrackParticle_phi; //!
  std::vector<float> TrackParticle_m; //!
  std::vector<float> TrackParticle_e; //!
  //std::vector<float> TrackParticle_et; //!
  std::vector<float> TrackParticle_rapidity; //!


  std::vector<float> TruthParticle_e; //!
  std::vector<float> TruthParticle_m; //!
  //std::vector<float> TruthParticle_polarizationPhi; //!
  //std::vector<float> TruthParticle_polarizationTheta; //!
  //std::vector<float> TruthParticle_px; //!
  //std::vector<float> TruthParticle_py; //!
  //std::vector<float> TruthParticle_pz; //!
  std::vector<float> TruthParticle_pt; //!
  std::vector<float> TruthParticle_eta; //!
  std::vector<float> TruthParticle_phi; //!
  std::vector<float> TruthParticle_rapidity; //!
  std::vector<int> TruthParticle_pdgId; //!

  CP::PileupReweightingTool*   mc_pileuptool; //!
  bool m_doPUreweighting;
  std::string mc_lumiCalcFileNames;
  std::string mc_PRWFileNames;

  bool m_applyGRLCut;
  std::string m_GRLxml;
  GoodRunsListSelectionTool*   m_grl;       //!

  float averageMu; //!
  float correctedAverageMu; //!
	//std::string m_configName;
	//std::string getConfig(bool expand=false);
	//void setConfig(std::string configName);

  // this is a standard constructor
  treeMaker ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  virtual EL::StatusCode configure ();
  // this is needed to distribute the algorithm to the workers
  ClassDef(treeMaker, 1);
};

#endif
