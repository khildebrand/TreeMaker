#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <RandomCone/treeMaker.h>
#include "xAODRootAccess/tools/Message.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
//#include "xAODCaloCluster/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
//#include "xAODCaloEvent/CaloCluster.h"

//#include <xAODAnaHelpers/HelperFunctions.h>

#include "TEnv.h"
#include "TFile.h"
//#include "TTree.h"
//#include "TTreeFormula.h"
#include "TSystem.h"
/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( CONTEXT,                                    \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
         return EL::StatusCode::FAILURE;                    \
      }                                                     \
   } while( false )

// this is needed to distribute the algorithm to the workers
ClassImp(treeMaker)



treeMaker :: treeMaker () :
    m_grl(nullptr),
	mc_pileuptool(nullptr)
{
	Info("treeMaker()", "Calling constructor");
	treeName ="";
	JetContainerName ="";
	JetContainerName2="";
	JetContainerName3 ="";
	JetContainerNameDoConstituent=false;
	JetContainerName2DoConstituent=false;
	JetContainerName3DoConstituent=false;
	CaloClusterContainerName="";
	PrimaryVertexContainerName="";
	TruthPrimaryVertexContainerName="";
	TrackParticleContainerName="";
	TruthParticleContainerName="";
	mc_lumiCalcFileNames = "";
    mc_PRWFileNames      = "";
    dataScaleFactor		 = 1.;
    dataScaleFactorUP		 = -1;
    dataScaleFactorDOWN		 = -1;
    m_doPUreweighting   = false;
    m_applyGRLCut = false;
    m_GRLxml = "";  //https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/GoodRunListsForAnalysis
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}
/*
std::string treeMaker ::getConfig(bool expand){
  if(expand) return gSystem->ExpandPathName( m_configName.c_str() );
  return m_configName;
}
*/
/*
void treeMaker::setConfig(std::string configName){
  m_configName = configName;
  if ( !m_configName.empty() ) {
    // check if file exists
     //https://root.cern.ch/root/roottalk/roottalk02/5332.html
    FileStat_t fStats;
    int fSuccess = gSystem->GetPathInfo(treeMaker::getConfig(true).c_str(), fStats);
    if(fSuccess != 0){
      // could not find
      RCU_THROW_MSG("xAH::Algorithm::setConfig could not find the config file specified:\r\n\t\tConfig File: "+configName);
    }
  }
}*/
EL::StatusCode treeMaker :: configure ()
{
	//Info("config()","path=%s",getConfig().c_str());
  if ( !getConfig().empty() ) {

    // read in user configuration from text file
    TEnv *config = new TEnv(getConfig(true).c_str());
    if ( !config ) {
      Error("treeMaker()", "Failed to initialize reading of config file. Exiting." );
      return EL::StatusCode::FAILURE;
    }

    // basics
    treeName = config->GetValue("treeName"     ,     treeName.c_str());
	JetContainerName = config->GetValue("JetContainerName"     ,     JetContainerName.c_str());
	JetContainerNameDoConstituent = config->GetValue("JetContainerNameDoConstituent", JetContainerNameDoConstituent);
	JetContainerName2 = config->GetValue("JetContainerName2"     ,     JetContainerName2.c_str());
	JetContainerName2DoConstituent = config->GetValue("JetContainerName2DoConstituent", JetContainerName2DoConstituent);
	JetContainerName3 = config->GetValue("JetContainerName3"     ,     JetContainerName3.c_str());
	JetContainerName3DoConstituent = config->GetValue("JetContainerName3DoConstituent", JetContainerName3DoConstituent);
	CaloClusterContainerName= config->GetValue("CaloClusterContainerName"     ,     CaloClusterContainerName.c_str());
	PrimaryVertexContainerName= config->GetValue("PrimaryVertexContainerName"     ,     PrimaryVertexContainerName.c_str());
	TruthPrimaryVertexContainerName= config->GetValue("TruthPrimaryVertexContainerName"     ,     TruthPrimaryVertexContainerName.c_str());
	TrackParticleContainerName= config->GetValue("TrackParticleContainerName"     ,     TrackParticleContainerName.c_str());
	TruthParticleContainerName= config->GetValue("TruthParticleContainerName"     ,     TruthParticleContainerName.c_str());

	mc_lumiCalcFileNames = config->GetValue("LumiCalcFiles",       mc_lumiCalcFileNames.c_str());
    mc_PRWFileNames      = config->GetValue("PRWFiles",            mc_PRWFileNames.c_str());
    dataScaleFactor      = config->GetValue("DataScaleFactor",            dataScaleFactor);
    dataScaleFactorUP      = config->GetValue("DataScaleFactorUP",            dataScaleFactorUP);
    dataScaleFactorDOWN      = config->GetValue("DataScaleFactorDOWN",            dataScaleFactorDOWN);
    m_doPUreweighting   = config->GetValue("DoPileupReweighting", m_doPUreweighting);
    m_applyGRLCut       = config->GetValue("ApplyGRLCut",        m_applyGRLCut);
    m_GRLxml            = config->GetValue("GRL", m_GRLxml.c_str());

    //Info("configure()","inContainer=%s",inContainer.c_str());
	//Info("configure()","treeName=%s",treeName.c_str());
    config->Print();
    Info("configure()", "treeMaker succesfully configured! ");
    delete config; config = nullptr;
  }

  if ( treeName.empty() ) {
    Error("configure()", "treeName is empty!");
    return EL::StatusCode::FAILURE;
  }
	if( m_doPUreweighting ){
	  if( mc_lumiCalcFileNames.size() == 0){
		Error("runMC2()", "Pileup Reweighting is requested but no LumiCalc file is specified. Exiting" );
		return EL::StatusCode::FAILURE;
	  }
	  if( mc_PRWFileNames.size() == 0){
		Error("runMC2()", "Pileup Reweighting is requested but no PRW file is specified. Exiting" );
		return EL::StatusCode::FAILURE;
	  }
	}
  return EL::StatusCode::SUCCESS;
}
EL::StatusCode treeMaker :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD ();
  EL::OutputStream outForTree("tree");
  job.outputAdd (outForTree);

  EL_RETURN_CHECK( "setupJob()", xAOD::Init() );
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode treeMaker :: histInitialize ()
{
  //configure
  this->configure();
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  // get the output file, create a new TTree and connect it to that output
  // define what braches will go in that tree
  TFile *outputFile = wk()->getOutputFile ("tree");
  tree = new TTree (treeName.c_str(), treeName.c_str());
  tree->SetDirectory (outputFile);
  tree->Branch("EventNumber", &EventNumber);

  if ( !JetContainerName.empty() ) {
	  tree->Branch((JetContainerName+"_width").c_str(), &Jet_width);
	  tree->Branch((JetContainerName+"_rapidity").c_str(), &Jet_rapidity);
	  tree->Branch((JetContainerName+"_e").c_str(), &Jet_e);
	  tree->Branch((JetContainerName+"_pt").c_str(), &Jet_pt);
	  tree->Branch((JetContainerName+"_eta").c_str(), &Jet_eta);
	  tree->Branch((JetContainerName+"_phi").c_str(), &Jet_phi);
	  tree->Branch((JetContainerName+"_m").c_str(), &Jet_m);
	  tree->Branch((JetContainerName+"_n").c_str(), &Jet_n);
	  if (JetContainerNameDoConstituent==true) {
		  tree->Branch((JetContainerName+"_Constituent_rapidity").c_str(), &JetConstituent_rapidity);
		  tree->Branch((JetContainerName+"_Constituent_e").c_str(), &JetConstituent_e);
		  tree->Branch((JetContainerName+"_Constituent_pt").c_str(), &JetConstituent_pt);
		  tree->Branch((JetContainerName+"_Constituent_eta").c_str(), &JetConstituent_eta);
		  tree->Branch((JetContainerName+"_Constituent_phi").c_str(), &JetConstituent_phi);
		  tree->Branch((JetContainerName+"_Constituent_m").c_str(), &JetConstituent_m);
	  }
  }
  if ( !JetContainerName3.empty() ) {
	  tree->Branch((JetContainerName3+"_width").c_str(), &Truth_Jet_width);
	  tree->Branch((JetContainerName3+"_rapidity").c_str(), &Truth_Jet_rapidity);
	  tree->Branch((JetContainerName3+"_e").c_str(), &Truth_Jet_e);
	  tree->Branch((JetContainerName3+"_pt").c_str(), &Truth_Jet_pt);
	  tree->Branch((JetContainerName3+"_eta").c_str(), &Truth_Jet_eta);
	  tree->Branch((JetContainerName3+"_phi").c_str(), &Truth_Jet_phi);
	  tree->Branch((JetContainerName3+"_m").c_str(), &Truth_Jet_m);
	  tree->Branch((JetContainerName3+"_n").c_str(), &Truth_Jet_n);
	  if (JetContainerName3DoConstituent==true) {
		  tree->Branch((JetContainerName3+"_Constituent_rapidity").c_str(), &Jet3Constituent_rapidity);
		  tree->Branch((JetContainerName3+"_Constituent_e").c_str(), &Jet3Constituent_e);
		  tree->Branch((JetContainerName3+"_Constituent_pt").c_str(), &Jet3Constituent_pt);
		  tree->Branch((JetContainerName3+"_Constituent_eta").c_str(), &Jet3Constituent_eta);
		  tree->Branch((JetContainerName3+"_Constituent_phi").c_str(), &Jet3Constituent_phi);
		  tree->Branch((JetContainerName3+"_Constituent_m").c_str(), &Jet3Constituent_m);
	  }
  }
  if ( !JetContainerName2.empty() ) {
	  tree->Branch((JetContainerName2+"_width").c_str(), &JetTk_width);
	  tree->Branch((JetContainerName2+"_rapidity").c_str(), &JetTk_rapidity);
	  tree->Branch((JetContainerName2+"_e").c_str(), &JetTk_e);
	  tree->Branch((JetContainerName2+"_pt").c_str(), &JetTk_pt);
	  tree->Branch((JetContainerName2+"_eta").c_str(), &JetTk_eta);
	  tree->Branch((JetContainerName2+"_phi").c_str(), &JetTk_phi);
	  tree->Branch((JetContainerName2+"_m").c_str(), &JetTk_m);
	  tree->Branch((JetContainerName2+"_n").c_str(), &JetTk_n);
	  if (JetContainerName2DoConstituent==true) {
		  tree->Branch((JetContainerName2+"_Constituent_rapidity").c_str(), &Jet2Constituent_rapidity);
		  tree->Branch((JetContainerName2+"_Constituent_e").c_str(), &Jet2Constituent_e);
		  tree->Branch((JetContainerName2+"_Constituent_pt").c_str(), &Jet2Constituent_pt);
		  tree->Branch((JetContainerName2+"_Constituent_eta").c_str(), &Jet2Constituent_eta);
		  tree->Branch((JetContainerName2+"_Constituent_phi").c_str(), &Jet2Constituent_phi);
		  tree->Branch((JetContainerName2+"_Constituent_m").c_str(), &Jet2Constituent_m);
	  }
  }
  if ( !CaloClusterContainerName.empty() ) {

	  tree->Branch((CaloClusterContainerName+"_EM_rapidity").c_str(), &CaloCluster_EM_rapidity);
	  tree->Branch((CaloClusterContainerName+"_EM_pt").c_str(), &CaloCluster_EM_pt);
	  tree->Branch((CaloClusterContainerName+"_EM_e").c_str(), &CaloCluster_EM_e);
	  tree->Branch((CaloClusterContainerName+"_EM_et").c_str(), &CaloCluster_EM_et);
	  tree->Branch((CaloClusterContainerName+"_EM_phi").c_str(), &CaloCluster_EM_phi);
	  tree->Branch((CaloClusterContainerName+"_EM_eta").c_str(), &CaloCluster_EM_eta);
	  tree->Branch((CaloClusterContainerName+"_EM_m").c_str(), &CaloCluster_EM_m);

	  tree->Branch((CaloClusterContainerName+"_LC_rapidity").c_str(), &CaloCluster_LC_rapidity);
	  tree->Branch((CaloClusterContainerName+"_LC_pt").c_str(), &CaloCluster_LC_pt);
	  tree->Branch((CaloClusterContainerName+"_LC_e").c_str(), &CaloCluster_LC_e);
	  tree->Branch((CaloClusterContainerName+"_LC_et").c_str(), &CaloCluster_LC_et);
	  tree->Branch((CaloClusterContainerName+"_LC_phi").c_str(), &CaloCluster_LC_phi);
	  tree->Branch((CaloClusterContainerName+"_LC_eta").c_str(), &CaloCluster_LC_eta);
	  tree->Branch((CaloClusterContainerName+"_LC_m").c_str(), &CaloCluster_LC_m);

	  tree->Branch((CaloClusterContainerName+"_n").c_str(), &CaloCluster_n);
  }
  if ( !PrimaryVertexContainerName.empty() ) {
	  tree->Branch((PrimaryVertexContainerName+"_nTracks").c_str(), &Primary_Vertex_nTracks);
	  tree->Branch((PrimaryVertexContainerName+"_n").c_str(), &Primary_Vertices_n);
	  //tree->Branch((PrimaryVertexContainerName+"_x", &Primary_Vertices_x);
	  //tree->Branch((PrimaryVertexContainerName+"_y", &Primary_Vertices_y);
	  tree->Branch((PrimaryVertexContainerName+"_z").c_str(), &Primary_Vertices_z);
	  tree->Branch((PrimaryVertexContainerName+"_type").c_str(), &Primary_Vertices_type);
  }
  if ( !TruthPrimaryVertexContainerName.empty() ) {
	  tree->Branch((TruthPrimaryVertexContainerName+"_n").c_str(), &Truth_Primary_Vertices_n);
	  tree->Branch((TruthPrimaryVertexContainerName+"_z").c_str(), &Truth_Primary_Vertices_z);
  }
  if ( !TrackParticleContainerName.empty() ) {
	  tree->Branch((TrackParticleContainerName+"_phi").c_str(), &TrackParticle_phi);
	  tree->Branch((TrackParticleContainerName+"_theta").c_str(), &TrackParticle_theta);
	  tree->Branch((TrackParticleContainerName+"_vz").c_str(), &TrackParticle_vz);
	  tree->Branch((TrackParticleContainerName+"_z0").c_str(), &TrackParticle_z0);
	  tree->Branch((TrackParticleContainerName+"_d0").c_str(), &TrackParticle_d0);
	  tree->Branch((TrackParticleContainerName+"_charge").c_str(), &TrackParticle_charge);

	  tree->Branch((TrackParticleContainerName+"_pt").c_str(), &TrackParticle_pt);
	  tree->Branch((TrackParticleContainerName+"_eta").c_str(), &TrackParticle_eta);
	  tree->Branch((TrackParticleContainerName+"_m").c_str(), &TrackParticle_m);
	  tree->Branch((TrackParticleContainerName+"_e").c_str(), &TrackParticle_e);
	  tree->Branch((TrackParticleContainerName+"_rapidity").c_str(), &TrackParticle_rapidity);
  }
  if ( !TruthParticleContainerName.empty() ) {
	  tree->Branch((TruthParticleContainerName+"_e").c_str(), &TruthParticle_e);
	  tree->Branch((TruthParticleContainerName+"_m").c_str(), &TruthParticle_m);
	  //tree->Branch((TruthParticleContainerName+"_polarizationPhi").c_str(), &TruthParticle_polarizationPhi);
	  //tree->Branch((TruthParticleContainerName+"_polarizationTheta").c_str(), &TruthParticle_polarizationTheta);
	  //tree->Branch((TruthParticleContainerName+"_px").c_str(), &TruthParticle_px);
	  //tree->Branch((TruthParticleContainerName+"_py").c_str(), &TruthParticle_py);
	  //tree->Branch((TruthParticleContainerName+"_pz").c_str(), &TruthParticle_pz);
	  tree->Branch((TruthParticleContainerName+"_pt").c_str(), &TruthParticle_pt);
	  tree->Branch((TruthParticleContainerName+"_eta").c_str(), &TruthParticle_eta);
	  tree->Branch((TruthParticleContainerName+"_rapidity").c_str(), &TruthParticle_rapidity);
	  tree->Branch((TruthParticleContainerName+"_pdgId").c_str(), &TruthParticle_pdgId);

  }
  tree->Branch("MCEventWeight", &EventWeight);
  tree->Branch("PileupWeight", &PileupWeight);
  tree->Branch("PileupWeight_Kevin", &PileupWeight_Kevin);
  tree->Branch("averageMu", &averageMu);
  tree->Branch("correctedAverageMu", &correctedAverageMu);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode treeMaker :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode treeMaker :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode treeMaker :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  // count number of events
  m_eventCounter = 0;

  xAOD::TEvent* event = wk()->xaodEvent();

  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  Info("initialize()", "Setting Up Tools");

  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  std::vector<std::string> vecStringGRL;
  m_GRLxml = gSystem->ExpandPathName( m_GRLxml.c_str() );
  vecStringGRL.push_back(m_GRLxml);
  EL_RETURN_CHECK("treeMaker::initialize()", m_grl->setProperty( "GoodRunsListVec", vecStringGRL));
  EL_RETURN_CHECK("treeMaker::initialize()", m_grl->setProperty("PassThrough", false));
  EL_RETURN_CHECK("treeMaker::initialize()", m_grl->initialize());
  if ( m_doPUreweighting ) {
    mc_pileuptool = new CP::PileupReweightingTool("Pileup2");

    std::vector<std::string> PRWFiles;
    std::vector<std::string> lumiCalcFiles;

    std::string tmp_lumiCalcFileNames = mc_lumiCalcFileNames;
    std::string tmp_PRWFileNames = mc_PRWFileNames;

    // Parse all comma seperated files
    while( tmp_PRWFileNames.size() > 0){
      int pos = tmp_PRWFileNames.find_first_of(',');
      if( pos == std::string::npos){
        pos = tmp_PRWFileNames.size();
        PRWFiles.push_back(tmp_PRWFileNames.substr(0, pos));
        tmp_PRWFileNames.erase(0, pos);
      }else{
        PRWFiles.push_back(tmp_PRWFileNames.substr(0, pos));
        tmp_PRWFileNames.erase(0, pos+1);
      }
    }
    while( tmp_lumiCalcFileNames.size() > 0){
      int pos = tmp_lumiCalcFileNames.find_first_of(',');
      if( pos == std::string::npos){
        pos = tmp_lumiCalcFileNames.size();
        lumiCalcFiles.push_back(tmp_lumiCalcFileNames.substr(0, pos));
        tmp_lumiCalcFileNames.erase(0, pos);
      }else{
        lumiCalcFiles.push_back(tmp_lumiCalcFileNames.substr(0, pos));
        tmp_lumiCalcFileNames.erase(0, pos+1);
      }
    }

    std::cout << "PileupReweighting Tool is adding Pileup files:" << std::endl;
    for( unsigned int i=0; i < PRWFiles.size(); ++i){
      std::cout << "    " << PRWFiles.at(i) << std::endl;
    }
    std::cout << "PileupReweighting Tool is adding Lumi Calc files:" << std::endl;
    for( unsigned int i=0; i < lumiCalcFiles.size(); ++i){
      std::cout << "    " << lumiCalcFiles.at(i) << std::endl;
    }
    std::cout << " dataScaleFactor=   " << 1./dataScaleFactor << std::endl;
	EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("ConfigFiles", PRWFiles));
    EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("LumiCalcFiles", lumiCalcFiles));
    EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactor", 1./dataScaleFactor));
    if (dataScaleFactorUP<0) {
		EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactorUP", 0.));
	} else {
		EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactorUP", 1./dataScaleFactorUP));
	}
    if (dataScaleFactorDOWN<0) {
		EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactorDOWN", 0.));
	} else {
		EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactorDOWN", 1./dataScaleFactorDOWN));
	}
	EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->initialize());
  }


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode treeMaker :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  xAOD::TEvent* event = wk()->xaodEvent();

  // print every 100 events, so we know where we are:
  if( (m_eventCounter % 100) ==0 ) {
	  Info("execute()", "Event number = %i", m_eventCounter );
  }
  m_eventCounter++;
  //h_nEvents->Fill(1); //just keep track of number of events




  //----------------------------
  // Event information
  //---------------------------
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));
  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  bool isMC = false;
  EventNumber = eventInfo->eventNumber();
  averageMu = eventInfo->averageInteractionsPerCrossing();
  correctedAverageMu=averageMu;
  // // Event weighting
	 //Info("execute()", "correctedAverageMu = %f", correctedAverageMu );
  EventWeight=1;
  PileupWeight=1;
  if( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION)) {
      isMC = true;
  }
  if ( m_doPUreweighting ) {
	 mc_pileuptool->apply(*eventInfo,true);
	 static SG::AuxElement::ConstAccessor< float > pileupWeightAcc("PileupWeight");
	 if( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION)) {
	 	PileupWeight = pileupWeightAcc(*eventInfo) ;
	 }
	  //Info("execute()", "PileupWeight = %f", PileupWeight );
	 if( !eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION)) {
	 	static SG::AuxElement::ConstAccessor< float > correctedMuAcc("corrected_averageInteractionsPerCrossing");
   	 	correctedAverageMu=correctedMuAcc(*eventInfo) ;
	 }
   	 //Info("execute()", "correctedAverageMu = %f", correctedAverageMu );
  }
  if( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION)) {
      const std::vector< float > weights = eventInfo->mcEventWeights();
      if( weights.size() > 0 ){
		  EventWeight = weights[0];

	  }
	  /*
	  if ( m_doPUreweighting ) {
       mc_pileuptool->apply(*eventInfo);
       static SG::AuxElement::ConstAccessor< float > pileupWeightAcc("PileupWeight");
       PileupWeight = pileupWeightAcc(*eventInfo) ;
       static SG::AuxElement::ConstAccessor< float > correctedMuAcc("corrected_averageInteractionsPerCrossing");
	   correctedAverageMu=correctedMuAcc(*eventInfo) ;
	   Info("execute()", "correctedAverageMu = %f", correctedAverageMu );
      }
      */
      //eventWeight = eventInfo->mcEventWeight();
      //eventWeight = eventWeight*filtEff;
      //Info("execute()", "mc weight = %.2f", eventWeight);

  }
  if ( !isMC ) {
    // GRL
    if ( m_applyGRLCut ) {
      if ( !m_grl->passRunLB( *eventInfo ) ) {
        wk()->skipEvent();
        return EL::StatusCode::SUCCESS; // go to next event
      }
    }
  }
  //get jet container of interest
  if ( !JetContainerName.empty() ) {
	  const xAOD::JetContainer* jets = 0;
	  EL_RETURN_CHECK("execute()",event->retrieve( jets, JetContainerName ));
	  //Info("execute()", "  number of jets = %lu", jets->size());
  /*
  const xAOD::JetContainer* truthJets = 0;
  if (isMC) {
  	EL_RETURN_CHECK("execute()",event->retrieve( truthJets, inContainerTruth ));
  }*/
  // loop over the jets in the container
	  xAOD::JetContainer::const_iterator jet_itr = jets->begin();
	  xAOD::JetContainer::const_iterator jet_end = jets->end();
	  float width = 0;
	  //float cut=0;
	  int numberJets=0;
	  float ptSum=0;
	  //te=jets->e();

	  Jet_pt.clear();
	  Jet_width.clear();
	  Jet_e.clear();
	  Jet_rapidity.clear();
	  Jet_eta.clear();
	  Jet_phi.clear();
	  Jet_m.clear();

	  JetConstituent_pt.clear();
	  JetConstituent_m.clear();
	  JetConstituent_e.clear();
	  JetConstituent_rapidity.clear();
	  JetConstituent_phi.clear();
	  JetConstituent_eta.clear();

	  std::vector<float> constituent_pt;
	  std::vector<float> constituent_m;
	  std::vector<float> constituent_e;
	  std::vector<float> constituent_rapidity;
	  std::vector<float> constituent_phi;
	  std::vector<float> constituent_eta;
	  for( ; jet_itr != jet_end; ++jet_itr ) {
		   //Info("execute()", "  jet pt = %.2f GeV", ((*jet_itr)->pt() * 0.001)); // just to print out something
		   //cut on rapidity
		   numberJets++;
		   Jet_pt.push_back((*jet_itr)->pt());
		   /*if (isMC) {
			   ptSum+=(*jet_itr)->pt();
		   }*/
		   Jet_m.push_back((*jet_itr)->m());
		   Jet_e.push_back((*jet_itr)->e());
		   Jet_rapidity.push_back((*jet_itr)->rapidity());
			   if ( (*jet_itr)->isAvailable< float >( "Width") ) {
				   width = (*jet_itr)->getAttribute< float > ( "Width");
				   Jet_width.push_back(width);
			   } else {
				   Error("execute()", "Couldn't find width");
			   }
		   Jet_phi.push_back((*jet_itr)->phi());
		   Jet_eta.push_back((*jet_itr)->eta());
			if (JetContainerNameDoConstituent==true) {
				  //xAOD::JetConstituentVector	Jet3_Constituents = 0;
				  //Info("execute()", " heyo1");
				  xAOD::JetConstituentVector Jet_Constituents=(*jet_itr)->getConstituents();
				  //const xAOD::JetContainer* Truth_jets = 0;

				  //Info("execute()", " heyo2");
				  xAOD::JetConstituentVector::const_iterator Jet_Constituent_itr = Jet_Constituents.begin();
				  xAOD::JetConstituentVector::const_iterator Jet_Constituent_end = Jet_Constituents.end();
				  //Info("execute()", " heyo3");
				  constituent_pt.clear();
				  constituent_m.clear();
				  constituent_e.clear();
				  constituent_rapidity.clear();
				  constituent_phi.clear();
				  constituent_eta.clear();
				  //Info("execute()", " heyo4");

				  for( ; Jet_Constituent_itr != Jet_Constituent_end; ++Jet_Constituent_itr ) {
					  //Info("execute()", " heyo5");
					  //float tempPt=Jet3_Constituents[0]->pt();
					  //float tempPt=(*Jet3_Constituent_itr)->pt();
					  //Info("execute()", " heyo5b");
					  //Info("execute()", " %f",tempPt);
					   constituent_pt.push_back((*Jet_Constituent_itr)->pt());
					   //Info("execute()", " heyo6");
					   constituent_m.push_back((*Jet_Constituent_itr)->m());
					   constituent_e.push_back((*Jet_Constituent_itr)->e());
					   constituent_rapidity.push_back((*Jet_Constituent_itr)->rapidity());

					   constituent_phi.push_back((*Jet_Constituent_itr)->phi());
					   constituent_eta.push_back((*Jet_Constituent_itr)->eta());
				  }
				  //Info("execute()", " heyo7");
				  JetConstituent_pt.push_back(constituent_pt);
				  //Info("execute()", " heyo8");
				  JetConstituent_m.push_back(constituent_m);
				  JetConstituent_e.push_back(constituent_e);
				  JetConstituent_rapidity.push_back(constituent_rapidity);
				  JetConstituent_phi.push_back(constituent_phi);
				  JetConstituent_eta.push_back(constituent_eta);
		   }


	  }
	  Jet_n=numberJets;
  }
  if ( !JetContainerName3.empty() ) {
	  const xAOD::JetContainer* Truth_jets = 0;
	  EL_RETURN_CHECK("execute()",event->retrieve( Truth_jets, JetContainerName3 ));

	  xAOD::JetContainer::const_iterator Truth_jet_itr = Truth_jets->begin();
	  xAOD::JetContainer::const_iterator Truth_jet_end = Truth_jets->end();
		int nJets=0;
	  Truth_Jet_pt.clear();
	  Truth_Jet_width.clear();
	  Truth_Jet_e.clear();
	  Truth_Jet_rapidity.clear();
	  Truth_Jet_eta.clear();
	  Truth_Jet_phi.clear();
	  Truth_Jet_m.clear();
	  Jet3Constituent_pt.clear();
	  Jet3Constituent_m.clear();
	  Jet3Constituent_e.clear();
	  Jet3Constituent_rapidity.clear();
	  Jet3Constituent_phi.clear();
	  Jet3Constituent_eta.clear();

	  std::vector<float> constituent_pt;
	  std::vector<float> constituent_m;
	  std::vector<float> constituent_e;
	  std::vector<float> constituent_rapidity;
	  std::vector<float> constituent_phi;
	  std::vector<float> constituent_eta;
	  for( ; Truth_jet_itr != Truth_jet_end; ++Truth_jet_itr ) {
		  nJets++;
			Truth_Jet_pt.push_back((*Truth_jet_itr)->pt());
			Truth_Jet_m.push_back((*Truth_jet_itr)->m());
			Truth_Jet_e.push_back((*Truth_jet_itr)->e());
			Truth_Jet_rapidity.push_back((*Truth_jet_itr)->rapidity());

			Truth_Jet_phi.push_back((*Truth_jet_itr)->phi());
			Truth_Jet_eta.push_back((*Truth_jet_itr)->eta());
			if (JetContainerName3DoConstituent==true) {
				  //xAOD::JetConstituentVector	Jet3_Constituents = 0;
				  //Info("execute()", " heyo1");
				  xAOD::JetConstituentVector Jet3_Constituents=(*Truth_jet_itr)->getConstituents();
				  //const xAOD::JetContainer* Truth_jets = 0;

				  //Info("execute()", " heyo2");
				  xAOD::JetConstituentVector::const_iterator Jet3_Constituent_itr = Jet3_Constituents.begin();
				  xAOD::JetConstituentVector::const_iterator Jet3_Constituent_end = Jet3_Constituents.end();
				  //Info("execute()", " heyo3");
				  constituent_pt.clear();
				  constituent_m.clear();
				  constituent_e.clear();
				  constituent_rapidity.clear();
				  constituent_phi.clear();
				  constituent_eta.clear();
				  //Info("execute()", " heyo4");

				  //Info("execute()","%i",Truth_jets->size());
				  //Info("execute()","%i",Jet3_Constituents.size());
				  for( ; Jet3_Constituent_itr != Jet3_Constituent_end; ++Jet3_Constituent_itr ) {
					  //Info("execute()", " heyo5");
					  //float tempPt=Jet3_Constituents[0]->pt();
					  //float tempPt=(*Jet3_Constituent_itr)->pt();
					  //Info("execute()", " heyo5b");
					  //Info("execute()", " %f",tempPt);
					   constituent_pt.push_back((*Jet3_Constituent_itr)->pt());
					   //Info("execute()", " heyo6");
					   constituent_m.push_back((*Jet3_Constituent_itr)->m());
					   constituent_e.push_back((*Jet3_Constituent_itr)->e());
					   constituent_rapidity.push_back((*Jet3_Constituent_itr)->rapidity());

					   constituent_phi.push_back((*Jet3_Constituent_itr)->phi());
					   constituent_eta.push_back((*Jet3_Constituent_itr)->eta());
				  }
				  //Info("execute()", " heyo7");
				  Jet3Constituent_pt.push_back(constituent_pt);
				  //Info("execute()", " heyo8");
				  Jet3Constituent_m.push_back(constituent_m);
				  Jet3Constituent_e.push_back(constituent_e);
				  Jet3Constituent_rapidity.push_back(constituent_rapidity);
				  Jet3Constituent_phi.push_back(constituent_phi);
				  Jet3Constituent_eta.push_back(constituent_eta);
		   }
	  }
	Truth_Jet_n=nJets;
  }
  if ( !JetContainerName2.empty() ) {
	  const xAOD::JetContainer* jetsTk = 0;
	  EL_RETURN_CHECK("execute()",event->retrieve( jetsTk, JetContainerName2 ));
	  //Info("execute()", "  number of jets = %lu", jets->size());
  /*
  const xAOD::JetContainer* truthJets = 0;
  if (isMC) {
  	EL_RETURN_CHECK("execute()",event->retrieve( truthJets, inContainerTruth ));
  }*/
  // loop over the jets in the container
	  xAOD::JetContainer::const_iterator jetTk_itr = jetsTk->begin();
	  xAOD::JetContainer::const_iterator jetTk_end = jetsTk->end();
	  float widthTk = 0;
	  //float cut=0;
	  int numberJetsTk=0;
	  float ptSumTk=0;
	  //te=jets->e();

	  JetTk_pt.clear();
	  JetTk_width.clear();
	  JetTk_e.clear();
	  JetTk_rapidity.clear();
	  JetTk_eta.clear();
	  JetTk_phi.clear();
	  JetTk_m.clear();

	  Jet2Constituent_pt.clear();
	  Jet2Constituent_m.clear();
	  Jet2Constituent_e.clear();
	  Jet2Constituent_rapidity.clear();
	  Jet2Constituent_phi.clear();
	  Jet2Constituent_eta.clear();

	  std::vector<float> constituent_pt;
	  std::vector<float> constituent_m;
	  std::vector<float> constituent_e;
	  std::vector<float> constituent_rapidity;
	  std::vector<float> constituent_phi;
	  std::vector<float> constituent_eta;
	  for( ; jetTk_itr != jetTk_end; ++jetTk_itr ) {
		   //Info("execute()", "  jet pt = %.2f GeV", ((*jet_itr)->pt() * 0.001)); // just to print out something
		   //cut on rapidity
		   numberJetsTk++;
		   JetTk_pt.push_back((*jetTk_itr)->pt());
		   /*if (isMC) {
			   ptSum+=(*jet_itr)->pt();
		   }*/
		   JetTk_m.push_back((*jetTk_itr)->m());
		   JetTk_e.push_back((*jetTk_itr)->e());
		   JetTk_rapidity.push_back((*jetTk_itr)->rapidity());
			   if ( (*jetTk_itr)->isAvailable< float >( "Width") ) {
				   widthTk = (*jetTk_itr)->getAttribute< float > ( "Width");
				   JetTk_width.push_back(widthTk);
			   } else {
				   Error("execute()", "Couldn't find width");
			   }
		   JetTk_phi.push_back((*jetTk_itr)->phi());
		   JetTk_eta.push_back((*jetTk_itr)->eta());
			if (JetContainerName2DoConstituent==true) {
				  //xAOD::JetConstituentVector	Jet3_Constituents = 0;
				  //Info("execute()", " heyo1");
				  xAOD::JetConstituentVector Jet2_Constituents=(*jetTk_itr)->getConstituents();
				  //const xAOD::JetContainer* Truth_jets = 0;

				  //Info("execute()", " heyo2");
				  xAOD::JetConstituentVector::const_iterator Jet2_Constituent_itr = Jet2_Constituents.begin();
				  xAOD::JetConstituentVector::const_iterator Jet2_Constituent_end = Jet2_Constituents.end();
				  //Info("execute()", " heyo3");
				  constituent_pt.clear();
				  constituent_m.clear();
				  constituent_e.clear();
				  constituent_rapidity.clear();
				  constituent_phi.clear();
				  constituent_eta.clear();
				  //Info("execute()", " heyo4");

				  for( ; Jet2_Constituent_itr != Jet2_Constituent_end; ++Jet2_Constituent_itr ) {
					  //Info("execute()", " heyo5");
					  //float tempPt=Jet3_Constituents[0]->pt();
					  //float tempPt=(*Jet3_Constituent_itr)->pt();
					  //Info("execute()", " heyo5b");
					  //Info("execute()", " %f",tempPt);
					   constituent_pt.push_back((*Jet2_Constituent_itr)->pt());
					   //Info("execute()", " heyo6");
					   constituent_m.push_back((*Jet2_Constituent_itr)->m());
					   constituent_e.push_back((*Jet2_Constituent_itr)->e());
					   constituent_rapidity.push_back((*Jet2_Constituent_itr)->rapidity());

					   constituent_phi.push_back((*Jet2_Constituent_itr)->phi());
					   constituent_eta.push_back((*Jet2_Constituent_itr)->eta());
				  }
				  //Info("execute()", " heyo7");
				  Jet2Constituent_pt.push_back(constituent_pt);
				  //Info("execute()", " heyo8");
				  Jet2Constituent_m.push_back(constituent_m);
				  Jet2Constituent_e.push_back(constituent_e);
				  Jet2Constituent_rapidity.push_back(constituent_rapidity);
				  Jet2Constituent_phi.push_back(constituent_phi);
				  Jet2Constituent_eta.push_back(constituent_eta);
		   }


	  }
	  JetTk_n=numberJetsTk;
  }
  /*if (isMC) {
	  float ptAvg=ptSum/numberJets;
	  //std::cout<<"ptSum="<<ptSum<<"numberJets="<<numberJets<<"ptAvg="<<ptAvg<<std::endl;
	  if( truthJets->size() == 0 || (ptAvg / truthJets->at(0)->pt() > 1.4) ){
		  wk()->skipEvent();
		  return EL::StatusCode::SUCCESS;
	  }
  }*/

  if ( !CaloClusterContainerName.empty() ) {
	  //get calocluster container of interest
	  const xAOD::CaloClusterContainer* caloCluster = 0;
	  EL_RETURN_CHECK("execute()",event->retrieve( caloCluster, CaloClusterContainerName ));
	  //Info("execute()", "  number of jets = %lu", jets->size());

	  // loop over the jets in the container
	  xAOD::CaloClusterContainer::const_iterator caloCluster_itr = caloCluster->begin();
	  xAOD::CaloClusterContainer::const_iterator caloCluster_end = caloCluster->end();
	  int numberCaloClusters=0;

	  //te=jets->e();
	  CaloCluster_EM_pt.clear();
	  CaloCluster_EM_e.clear();
	  CaloCluster_EM_et.clear();
	  CaloCluster_EM_phi.clear();
	  CaloCluster_EM_eta.clear();
	  CaloCluster_EM_m.clear();
	  CaloCluster_EM_rapidity.clear();

	  CaloCluster_LC_pt.clear();
	  CaloCluster_LC_e.clear();
	  CaloCluster_LC_et.clear();
	  CaloCluster_LC_phi.clear();
	  CaloCluster_LC_eta.clear();
	  CaloCluster_LC_m.clear();
	  CaloCluster_LC_rapidity.clear();

	  for( ; caloCluster_itr != caloCluster_end; ++caloCluster_itr ) {
		  //if ( (*caloCluster_itr)->pt( xAOD::CaloCluster::State::UNCALIBRATED ) < 2000 ) { continue; } // 2 GeV cut
		   //Info("execute()", "  jet pt = %.2f GeV", ((*jet_itr)->pt() * 0.001)); // just to print out something
		   numberCaloClusters++;
		   (*caloCluster_itr)->setSignalState(xAOD::CaloCluster::State::UNCALIBRATED);
		   CaloCluster_EM_pt. push_back( (*caloCluster_itr)->pt ( xAOD::CaloCluster::State::UNCALIBRATED ) );
		   CaloCluster_EM_e. push_back( (*caloCluster_itr)->e ( xAOD::CaloCluster::State::UNCALIBRATED ) );
		   CaloCluster_EM_et. push_back( (*caloCluster_itr)->et ()  );
		   CaloCluster_EM_m. push_back( (*caloCluster_itr)->m ()  );
		   CaloCluster_EM_rapidity. push_back( (*caloCluster_itr)->rapidity () );
		   CaloCluster_EM_phi. push_back( (*caloCluster_itr)->phi ( xAOD::CaloCluster::State::UNCALIBRATED ) );
		   CaloCluster_EM_eta. push_back( (*caloCluster_itr)->eta ( xAOD::CaloCluster::State::UNCALIBRATED ) );

		   (*caloCluster_itr)->setSignalState(xAOD::CaloCluster::State::CALIBRATED);
		   CaloCluster_LC_pt. push_back( (*caloCluster_itr)->pt ( xAOD::CaloCluster::State::CALIBRATED ) );
		   CaloCluster_LC_e. push_back( (*caloCluster_itr)->e ( xAOD::CaloCluster::State::CALIBRATED ) );
		   CaloCluster_LC_et. push_back( (*caloCluster_itr)->et ()  );
		   CaloCluster_LC_m. push_back( (*caloCluster_itr)->m ()  );
		   CaloCluster_LC_rapidity. push_back( (*caloCluster_itr)->rapidity () );
		   CaloCluster_LC_phi. push_back( (*caloCluster_itr)->phi ( xAOD::CaloCluster::State::CALIBRATED ) );
		   CaloCluster_LC_eta. push_back( (*caloCluster_itr)->eta ( xAOD::CaloCluster::State::CALIBRATED ) );


	  }
	  CaloCluster_n=numberCaloClusters;
  }
  if ( !PrimaryVertexContainerName.empty() ) {
	  const xAOD::VertexContainer* vertices=0;
	  EL_RETURN_CHECK("execute()",event->retrieve( vertices, PrimaryVertexContainerName ));
	  //int location =HelperFunctions::getPrimaryVertexLocation(vertices);

	  xAOD::VertexContainer::const_iterator vertices_itr = vertices->begin();
	  xAOD::VertexContainer::const_iterator vertices_end = vertices->end();
 	  int numberVertices=0;
 	  //const xAOD::Vertex* primaryVertex = HelperFunctions::getPrimaryVertex( vertices );
 	  //Primary_Vertex_nTracks=(int)(primaryVertex)->nTrackParticles();

 	  Primary_Vertex_nTracks.clear();
 	 // Primary_Vertices_x.clear();
 	 // Primary_Vertices_y.clear();
 	  Primary_Vertices_z.clear();
 	  Primary_Vertices_type.clear();
 	  float temp;
	  for( ; vertices_itr != vertices_end; ++vertices_itr ) {

		  //Primary_Vertices_x.push_back((*vertices_itr)->x());
		  //Primary_Vertices_y.push_back((*vertices_itr)->y());
		  Primary_Vertices_z.push_back((*vertices_itr)->z());
		  Primary_Vertices_type.push_back((*vertices_itr)->vertexType());
		  //if((*vertices_itr)->vertexType() != xAOD::VxType::VertexType::PriVtx) { continue; }
		  numberVertices++;
		  Primary_Vertex_nTracks. push_back((int)(*vertices_itr)->nTrackParticles()  );
	  }

	  Primary_Vertices_n=numberVertices;
  }
  if ( !TruthPrimaryVertexContainerName.empty() ) {
	  const xAOD::TruthVertexContainer* vertices=0;
	  EL_RETURN_CHECK("execute()",event->retrieve( vertices, TruthPrimaryVertexContainerName ));
	  //int location =HelperFunctions::getPrimaryVertexLocation(vertices);

	  xAOD::TruthVertexContainer::const_iterator vertices_itr = vertices->begin();
	  xAOD::TruthVertexContainer::const_iterator vertices_end = vertices->end();
 	  int numberVertices=0;
 	  //const xAOD::Vertex* primaryVertex = HelperFunctions::getPrimaryVertex( vertices );
 	  //Primary_Vertex_nTracks=(int)(primaryVertex)->nTrackParticles();

 	  Truth_Primary_Vertices_z.clear();

 	  float temp;
	  for( ; vertices_itr != vertices_end; ++vertices_itr ) {

		  //Primary_Vertices_x.push_back((*vertices_itr)->x());
		  //Primary_Vertices_y.push_back((*vertices_itr)->y());
		  Truth_Primary_Vertices_z.push_back((*vertices_itr)->z());

		  //if((*vertices_itr)->vertexType() != xAOD::VxType::VertexType::PriVtx) { continue; }
		  numberVertices++;
	  }

	  Truth_Primary_Vertices_n=numberVertices;
  }
  if ( !TrackParticleContainerName.empty() ) {
	  const xAOD::TrackParticleContainer* tracks=0;
	  EL_RETURN_CHECK("execute()",event->retrieve( tracks, TrackParticleContainerName ));

	  xAOD::TrackParticleContainer::const_iterator tracks_itr = tracks->begin();
	  xAOD::TrackParticleContainer::const_iterator tracks_end = tracks->end();

	  TrackParticle_phi.clear();
	  TrackParticle_theta.clear();
	  TrackParticle_vz.clear();
	  TrackParticle_z0.clear();
	  TrackParticle_d0.clear();
	  TrackParticle_charge.clear();

	  TrackParticle_pt.clear();
	  TrackParticle_eta.clear();
	  TrackParticle_m.clear();
	  TrackParticle_e.clear();
	  //TrackParticle_et.clear();
	  TrackParticle_rapidity.clear();
	  for( ; tracks_itr != tracks_end; ++tracks_itr ) {
		  TrackParticle_phi.push_back((*tracks_itr)->phi());
		  TrackParticle_theta.push_back((*tracks_itr)->theta());
		  TrackParticle_vz.push_back((*tracks_itr)->vz());
		  TrackParticle_z0.push_back((*tracks_itr)->z0());
		  TrackParticle_d0.push_back((*tracks_itr)->d0());
		  TrackParticle_charge.push_back((*tracks_itr)->charge());

		  TrackParticle_pt.push_back((*tracks_itr)->pt());
		  TrackParticle_eta.push_back((*tracks_itr)->eta());
		  TrackParticle_m.push_back((*tracks_itr)->m());
		  TrackParticle_e.push_back((*tracks_itr)->e());
		  //TrackParticle_et.push_back((*tracks_itr)->et());
		  TrackParticle_rapidity.push_back((*tracks_itr)->rapidity());
	  }

  }
  if ( !TruthParticleContainerName.empty() ) {
	  const xAOD::TruthParticleContainer* truths=0;
	  EL_RETURN_CHECK("execute()",event->retrieve( truths, TruthParticleContainerName ));

	  xAOD::TruthParticleContainer::const_iterator truths_itr = truths->begin();
	  xAOD::TruthParticleContainer::const_iterator truths_end = truths->end();

	  TruthParticle_e.clear();
	  TruthParticle_m.clear();
	  //TruthParticle_polarizationPhi.clear();
	  //TruthParticle_polarizationTheta.clear();
	  TruthParticle_pt.clear();
	  TruthParticle_eta.clear();
	  TruthParticle_phi.clear();
	  TruthParticle_rapidity.clear();
	  TruthParticle_pdgId.clear();
	  for( ; truths_itr != truths_end; ++truths_itr ) {
		  TruthParticle_e.push_back((*truths_itr)->e());
		  TruthParticle_m.push_back((*truths_itr)->m());
		  TruthParticle_pt.push_back((*truths_itr)->pt());
		  TruthParticle_eta.push_back((*truths_itr)->eta());
		  TruthParticle_phi.push_back((*truths_itr)->phi());
		  TruthParticle_rapidity.push_back((*truths_itr)->rapidity());
		  TruthParticle_pdgId.push_back((*truths_itr)->pdgId());
	  }
  }
  tree->Fill();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode treeMaker :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode treeMaker :: finalize ()
{
	xAOD::TEvent* event = wk()->xaodEvent();
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  if(mc_pileuptool) delete mc_pileuptool;
  if(m_grl) delete m_grl;
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode treeMaker :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
