void steerTreeMaker (const std::string& submitDir,const std::string& version)
{
  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================
  bool runOnGrid=true;

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.
  if (runOnGrid==false) {
	  SH::readFileList (sh, "sample", "/home/khildebr/qualTask/filelist/filelist.txt");
  } else {

		//used for previous run throughs
	  SH::scanRucio (sh, "mc15_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.merge.DAOD_JETM5.e3569_s2576_s2132_r7267_r6282_p2530");
	  SH::scanRucio (sh, "mc15_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.DAOD_JETM5.e3569_s2576_s2132_r7267_r6282_p2530");

  }
  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");
  sh.setMetaString("nc_grid_filter", "*");  //Data files on grid to not end in .root
  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job


  job.options()->setDouble (EL::Job::optCacheSize, 10*1024*1024); //Setup TTreeCache to make it run quicker

  if (runOnGrid==false){
  	job.options()->setDouble (EL::Job::optMaxEvents, 1000);
  	job.options()->setDouble (EL::Job::optRemoveSubmitDir, 1);
  }
  // add our algorithm to the job
  treeMaker *alg = new treeMaker;
  alg->setName("runMCC")->setConfig("$ROOTCOREBIN/data/RandomCone/tree.config");


  job.algsAdd(alg);

  if (runOnGrid==false) {
    EL::DirectDriver driver;
    driver.options()->setString("nc_outputSampleName", "user.khildebr.test.%in:name[2]%"+version);
    driver.submit (job, submitDir);
  } else {
	  EL::PrunDriver driver;

	  driver.options()->setString("nc_outputSampleName", "user.khildebr.test.%in:name[2]%.%in:name[4]%.%in:name[5]%"+version);
	  driver.submitOnly (job, submitDir);
  }

}