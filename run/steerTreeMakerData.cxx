void steerTreeMakerData (const std::string& submitDir,const std::string& version)
{
  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================

  bool runOnGrid=true;
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:

  if (runOnGrid==false) {
	  SH::readFileList (sh, "sample", "/home/khildebr/qualTask/filelist/filelistData.txt");
  } else {
  	  SH::scanRucio (sh,"data15_13TeV.00271388.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279928.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00281075.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00267162.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279515.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00266904.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00266919.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276245.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276161.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276330.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276790.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276954.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279279.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280368.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00278748.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280500.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279345.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276731.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280464.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276181.physics_ZeroBias.merge.DAOD_JETM5.r7567_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00284154.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00284006.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279685.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00283780.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00284213.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00281143.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00270949.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279932.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276147.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00283155.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279169.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276778.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279598.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279284.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280977.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276212.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280950.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276511.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00271421.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00284285.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00278880.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00282784.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00267638.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00267073.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00284473.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00270588.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00281074.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00278970.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280423.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00281381.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00281385.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00267639.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00283074.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00283429.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00281130.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280862.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00270816.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00271048.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00270953.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00278727.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00278968.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279764.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00283608.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00270806.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276689.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00284427.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276189.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276336.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00278912.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280231.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279259.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279984.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280520.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00284420.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00281317.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00281411.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276952.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276329.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00270448.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00267167.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280853.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00282712.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00271744.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276416.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276183.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279813.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00270441.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00283270.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00271516.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276262.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280673.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280753.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00282631.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00279867.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276073.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00282625.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00271595.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280319.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00284484.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00282992.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280614.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00276176.physics_ZeroBias.merge.DAOD_JETM5.r7567_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00267152.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00280273.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00271298.physics_ZeroBias.merge.DAOD_JETM5.r7600_p2521_p2614/");
  	  SH::scanRucio (sh,"data15_13TeV.00281070.physics_ZeroBias.merge.DAOD_JETM5.r7562_p2521_p2614/");

  }


  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");
  sh.setMetaString("nc_grid_filter", "*");  //Data files on grid to not end in .root
  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job

  job.options()->setDouble (EL::Job::optCacheSize, 10*1024*1024); //Setup TTreeCache to make it run quicker

  // option used for setting the maximum number of events to process per sample
  if (runOnGrid==false){
  	job.options()->setDouble (EL::Job::optMaxEvents, 1000);
  	job.options()->setDouble (EL::Job::optRemoveSubmitDir, 1); //automatically remove submit directory if it already exists.
  }

  // add our algorithm to the job
  treeMaker *alg = new treeMaker;
  alg->setName("runData")->setConfig("$ROOTCOREBIN/data/RandomCone/tree.config");
  job.algsAdd(alg);

  if (runOnGrid==false) {
    EL::DirectDriver driver;
    driver.options()->setString("nc_outputSampleName", "user.khildebr.test.%in:name[2]%"+version);
    driver.submit (job, submitDir);
  } else {

 	 EL::PrunDriver driver;
 	 driver.options()->setString("nc_outputSampleName", "user.khildebr.test.%in:name[2]%.%in:name[3]%"+version);
 	 driver.submitOnly (job, submitDir);
  }

}